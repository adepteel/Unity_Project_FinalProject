using UnityEngine.UI;
using UnityEngine;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using System.Net;
using System.Net.Sockets;
using System;

namespace UI
{
    public class NetCodeUI : MonoBehaviour
    {
        [SerializeField] private Button hostBtn;
        [SerializeField] private Button serverBtn;
        [SerializeField] private Button clientBtn;
        [SerializeField] private GameObject test;
        [SerializeField] private InputField inputAddress;

        private void Start()
        {
            hostBtn.onClick.AddListener(() =>
            {
                //UnityTransport transport = NetworkManager.Singleton.gameObject.GetComponent<UnityTransport>();
                //transport.SetConnectionData(GetLocalIPAddress(), 7777);
                NetworkManager.Singleton.StartHost();
                Application.targetFrameRate = 60;
                SpawnTest();
            });
            serverBtn.onClick.AddListener(() =>
            {
                //UnityTransport transport = NetworkManager.Singleton.gameObject.GetComponent<UnityTransport>();
                //transport.SetConnectionData(GetLocalIPAddress(), 7777);
                NetworkManager.Singleton.StartServer();
                Application.targetFrameRate = 60;
                SpawnTest();
            });
            clientBtn.onClick.AddListener(() =>
            {
                UnityTransport transport = NetworkManager.Singleton.gameObject.GetComponent<UnityTransport>();
                string port = inputAddress.text;
                if (inputAddress.text == null || inputAddress.text.Equals(string.Empty))
                {
                    port = transport.ConnectionData.Address;
                }
                transport.SetConnectionData(port, 7777);
                if(NetworkManager.Singleton.StartClient() == false)
                {
                    Debug.Log("Failed to connect!");
                }
            });   
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork && ip.ToString().Substring(0,3) == "192")
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        private void SpawnTest()
        {
            test = Instantiate(test, new Vector3(0, 0, 10), Quaternion.identity);
            test.GetComponent<NetworkObject>().Spawn(true);
        }
    }
}

