using Gameplay;
using UnityEngine;

public class Control : MonoBehaviour, ITeammate
{
    Animator a;
    float directionX, directionZ;
    CharacterController charachterController;
    Vector3 moveDirection = Vector3.zero;
    bool jump;
    float speed;

    float gravity = 1.98f;

    [SerializeField]
    private Belonging team;
    float jumpHeight = 0.5f;

    public Belonging Side { get => team; set => team = value; }

    void Start()
    {
        //a = GetComponent<Animator>();
        charachterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        speed = 10;
        directionX = Input.GetAxis("Horizontal");
        directionZ = Input.GetAxis("Vertical");
        moveDirection = new Vector3(directionX, 0, directionZ);
        a?.SetFloat("HorizontalInput", directionX);
        a?.SetFloat("VerticalInput", directionZ);
        if (!jump && Input.GetKeyDown(KeyCode.Space))
            jump = true;
    }


    private void FixedUpdate()
    {
        Vector3 move = moveDirection * 3f * Time.fixedDeltaTime;
        if (jump)
        {
            move.y = jumpHeight;
            jump = false;
        }

        if (jump && charachterController.isGrounded)
            move.y = 0;
        else
            move.y -= gravity * Time.fixedDeltaTime;
        charachterController.Move(move);
    }
}
