/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    CharacterController characterController;
    float directionX, directionZ;
    Vector3 movement = Vector3.zero;
    Vector3 moveDirection = Vector3.zero;

    [SerializeField] float speed;
    [SerializeField] float gravity;
    [SerializeField] float jumpHeight;

    bool jump;

    void Start()
    {
        speed = 10;
        jump = false;
        characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        directionX = Input.GetAxis("Horizontal");
        directionZ = Input.GetAxis("Vertical");
        moveDirection = new Vector3(directionX, 0, directionZ);

        if (!jump && Input.GetKeyDown("space"))
        {
            if (characterController.isGrounded)
                jump = true;
        }

    }
        
    private void FixedUpdate()
    {
        movement = moveDirection * speed * Time.fixedDeltaTime;

        if (jump)
        {
            movement.y = jumpHeight;
            jump = false;
        }

        if (jump && characterController.isGrounded)
            movement.y = 0;
        else
            movement.y = gravity;
        
        characterController.Move(movement);
    }  
}
*/

using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    Animator a;
    CharacterController charchterController;
    float directionX, directionZ;

    Vector3 movement = Vector3.zero;
    Vector3 moveDirection = Vector3.zero;

    [SerializeField]
    float speed;

    [SerializeField]
    float gravity;

    [SerializeField]
    float jumpHeight;

    bool jump;

    void Start()
    {
        speed = 10;
        jump = false;
        charchterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        directionX = Input.GetAxis("Horizontal");
        directionZ = Input.GetAxis("Vertical");
        moveDirection = new Vector3(directionX, 0, directionZ);

        a.SetFloat("Horizontal", directionX);
        a.SetFloat("Vertical", directionZ);

        if (!jump && Input.GetKeyDown(KeyCode.Space))
        {
            if (charchterController.isGrounded)
                jump = true;
        }

    }

    private void FixedUpdate()
    {
        movement = moveDirection * speed * Time.fixedDeltaTime;

        if (jump)
        {
            movement.y = jumpHeight;
            jump = false;
        }

        if (jump && charchterController.isGrounded)
            movement.y = 0;
        else
            movement.y -= gravity;

        charchterController.Move(movement);

    }
}

