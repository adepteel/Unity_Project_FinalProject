using System.Collections.Generic;
using UnityEngine;
using Gameplay.Weapons;

[CreateAssetMenu(fileName = "Player")]
public class Player : ScriptableObject
{
    public List<Weapon> weaponPrefabs;
}
