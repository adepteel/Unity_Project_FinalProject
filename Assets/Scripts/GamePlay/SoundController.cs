using Unity.Netcode;
using UnityEngine;

namespace Gameplay
{
    using Weapons;

    public class SoundController : NetworkBehaviour
    {
        private WeaponConnector weaponConnector;

        private void Start()
        {
            weaponConnector = new WeaponConnector(GetComponent<WeaponController>(), OnWeaponAttackedClientRpc);
        }

        [ClientRpc]
        private void OnWeaponAttackedClientRpc(string attackName)
        {
            Debug.Log($"Weapon sound of {NetworkObject.OwnerClientId} Player recived the following: {attackName}");
        }

        public override void OnDestroy()
        {
            weaponConnector.Disconnect();
            base.OnDestroy();
        }
    }
}
