using Unity.Netcode;
using UnityEngine;

namespace Gameplay
{
    using  Weapons;
    
    public class AnimationController : NetworkBehaviour
    {
        private const string HORIZONTAL = "Horizontal";
        private const string VERTICAL = "Vertical";
        private const float SPRINT_START_VALUE = 0.5F;
        private Animator animator;
        private MovementNetcode movement;
        private WeaponConnector weaponConnector;

        private void Start()
        {
            movement = GetComponent<MovementNetcode>();
            weaponConnector = new WeaponConnector(GetComponent<WeaponController>(), OnWeaponAttackedClientRpc, OnDistanceWeaponReloadedClientRpc);
        }

        private void FixedUpdate()
        {
            Vector3 input = movement.InputMovement;
            animator?.SetFloat(HORIZONTAL, movement.IsSprinting ? input.x : Mathf.Clamp(input.x, 0, SPRINT_START_VALUE));
            animator?.SetFloat(VERTICAL, movement.IsSprinting ? input.z : Mathf.Clamp(input.z, 0, SPRINT_START_VALUE));
        }

        [ClientRpc]
        private void OnWeaponAttackedClientRpc(string attackName)
        {
            Debug.Log($"Animator {NetworkObject.OwnerClientId} recived the following: {attackName}");
            animator?.SetTrigger(attackName);
        }

        [ClientRpc]
        private void OnDistanceWeaponReloadedClientRpc(string reloadName)
        {
            Debug.Log($"Animator {NetworkObject.OwnerClientId} recived the following: {reloadName}");
            animator?.SetTrigger(reloadName);
        }

        public override void OnDestroy()
        {
            weaponConnector.Disconnect();
            base.OnDestroy();
        }
    }

}
