using Cinemachine;
using Unity.Netcode;
using UnityEngine;

namespace Gameplay
{
    public class MovementNetcode : NetworkBehaviour
    {
        [Range(0f, 1f)]
        [SerializeField]
        private float rotaionSpeed;
        [SerializeField] private float movementSpeed;
        [SerializeField] private float sprintSpeed;
        [SerializeField] private CinemachineVirtualCamera playerCamera;
        private Vector3 input;
        private Quaternion initialCameraRotation;
        private bool isSprinting;
        private KeyCode sprintKey;

        private void Start()
        {
            sprintKey = KeyCode.LeftShift;
            input = Vector3.zero;
            initialCameraRotation = playerCamera.transform.rotation;
            playerCamera.gameObject.transform.parent = null;
            if (!IsLocalPlayer)
            {
                playerCamera.enabled = false;
            }
        }

        private void Update()
        {
            input.x = Input.GetAxis("Horizontal");
            input.z = Input.GetAxis("Vertical");
            isSprinting = Input.GetKey(sprintKey);
        }

        private void FixedUpdate()
        {
            if (IsClient)
            {
                playerCamera.transform.rotation = initialCameraRotation;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                ServerRpcParams serverRpcParams = new ServerRpcParams();
                serverRpcParams.Receive.SenderClientId = NetworkManager.LocalClientId;
                Vector3 hitPoint = transform.position;
                if (Physics.Raycast(ray, out RaycastHit hit, 100f))
                {
                    hitPoint = hit.point;
                }
                MoveServerRpc(hitPoint, input, serverRpcParams);
            }
        }

        [ServerRpc(RequireOwnership = false)]
        private void MoveServerRpc(Vector3 hit, Vector3 input, ServerRpcParams @params)
        {
            ulong cliendId = @params.Receive.SenderClientId;
            if (NetworkManager.ConnectedClients.ContainsKey(cliendId))
            {
                NetworkClient client = NetworkManager.ConnectedClients[cliendId];
                Transform temp = client.PlayerObject.transform;
                Rotate(temp, hit);
                Move(temp, input);
            }
        }

        private void Move(Transform temp, in Vector3 input)
        {
            if (input != Vector3.zero)
            {
                Vector3 movement = temp.forward * input.z + temp.right * input.x;
                temp.position += movement * Time.fixedDeltaTime * (isSprinting ? sprintSpeed : movementSpeed);
            }
        }

        private void Rotate(Transform temp, in Vector3 hit)
        {
            Vector3 rotation = hit - transform.position;
            if (rotation != Vector3.zero)
            {
                float angle = Mathf.Atan2(rotation.x, rotation.z) * Mathf.Rad2Deg;
                temp.rotation = Quaternion.Lerp(temp.rotation, Quaternion.AngleAxis(angle, temp.up), rotaionSpeed);
            }
        }

        public bool IsSprinting => isSprinting;

        public KeyCode SprintKey
        {
            get => sprintKey;
            set => sprintKey = value;
        }

        public Vector3 InputMovement => input;
    }
}

