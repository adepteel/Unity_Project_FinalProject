using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay
{
    public enum Belonging
    {
        None,
        Red,
        FullRed,
        Blue,
        FullBlue
    }

    public class KeyPoint : MonoBehaviour
    {
        private const int TO_FULL_BELONGING_SHIFT = 1;

        [SerializeField]
        [CustomAttributes.ReadOnlyField]
        private int numberOfBluePlayers;
        [SerializeField]
        [CustomAttributes.ReadOnlyField]
        private int numberOfRedPlayers;
        [SerializeField]
        [CustomAttributes.ReadOnlyField]
        private Belonging currentStatus;
        [SerializeField] private Image imagePoint;
        [SerializeField]
        [Tooltip("Expressed in seconds")]
        private float timeToCapture;
        [SerializeField]
        [Tooltip("Expressed in seconds")]
        private float fillingPeriod;
        private float step;

        private void Start()
        {
            step = fillingPeriod / timeToCapture;
        }

        private void OnTriggerEnter(Collider other)
        {
            ClashForPoint(other.GetComponent<ITeammate>(), false);
            StartCoroutine(Capture());
        }

        private void OnTriggerExit(Collider other)
        {
            ClashForPoint(other.GetComponent<ITeammate>(), true);
        }

        private void ClashForPoint(ITeammate teammate, in bool isLeaving)
        {
            int shift = isLeaving ? -1 : 1;
            switch (teammate.Side)
            {
                case Belonging.Red:
                    numberOfRedPlayers += shift;
                    break;
                case Belonging.Blue:
                    numberOfBluePlayers += shift;
                    break;
            }
        }

        private IEnumerator Capture()
        {
            Debug.Log("Started!");
            while ((numberOfBluePlayers == 0 && numberOfRedPlayers == 0) == false)
            {
                Debug.Log("Clashing");
                if (numberOfBluePlayers != numberOfRedPlayers)
                {
                    Clash(numberOfBluePlayers > numberOfRedPlayers ? Belonging.Blue : Belonging.Red);
                }
                yield return new WaitForSeconds(step);
            }
            Debug.Log("Ended");
        }

        private void Clash(Belonging allySide)
        {
            Color side = allySide == Belonging.Blue ? new Color(0, 0, 1, 1) : new Color(1, 0, 0, 1);
            if (currentStatus == Belonging.None)
            {
                currentStatus = allySide;
                imagePoint.color = side;
            }
            if (currentStatus == allySide || currentStatus == allySide + TO_FULL_BELONGING_SHIFT)
            {
                imagePoint.fillAmount += step;

                if (imagePoint.fillAmount == 1)
                    currentStatus = allySide + TO_FULL_BELONGING_SHIFT;
            }
            else
            {
                imagePoint.fillAmount -= step;
                if (IsCapturedTotaly())
                {
                    currentStatus -= TO_FULL_BELONGING_SHIFT;
                }
                if (imagePoint.fillAmount == 0)
                {
                    currentStatus = allySide;
                    imagePoint.color = side;
                }
            }
        }

        private bool IsCapturedTotaly() => currentStatus.ToString().StartsWith('F');

        public Belonging CurrentState => currentStatus;
    }

    public interface ITeammate
    {
        public Belonging Side { get; set; }
    }
}