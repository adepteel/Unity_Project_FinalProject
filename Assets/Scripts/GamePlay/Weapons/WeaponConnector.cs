using UnityEngine.Events;

namespace Gameplay
{
    namespace Weapons
    {
        public class WeaponConnector 
        {
            private WeaponController controller;
            private UnityAction<string> attackHandler;
            private UnityAction<string> reloadingHandler;

            /// <summary>
            /// Create a link to the Weapon Controller of the Player
            /// </summary>
            /// <param name="controller">The Weapon Controller of the Player</param>
            /// <param name="attackHandler">The handler should be ClientRpc</param>
            public WeaponConnector(WeaponController controller, UnityAction<string> attackHandler)
            {
                this.attackHandler = attackHandler;
                this.controller = controller;
                this.controller.WeaponChanched += OnPickedUp;
            }

            /// <summary>
            /// Create a link to the Weapon Controller of the Player
            /// </summary>
            /// <param name="controller">The Weapon Controller of the Player</param>
            /// <param name="attackHandler">Called when weapon execute attack. The handler should be ClientRpc</param>
            /// <param name="reloadingHandler">Called when distance weapon execute reloading. The handler should be ClientRpc. OPTIONAL</param>
            public WeaponConnector(WeaponController controller, UnityAction<string> attackHandler, UnityAction<string> reloadingHandler) : this(controller, attackHandler)
            {
                this.reloadingHandler = reloadingHandler;
            }

            private void OnPickedUp(Weapon previouWeapon)
            {
                DetachWeapon(previouWeapon);
                AttachWeapon(controller.CurrentWeapon);
            }

            private void AttachWeapon(Weapon weapon)
            {
                if (weapon)
                {
                    weapon.Attacked += attackHandler;
                    DistanceWeapon distanceWeapon = weapon as DistanceWeapon;
                    if (distanceWeapon != null)
                    {
                        distanceWeapon.MagazineReloaded += reloadingHandler;
                    }
                }
            }

            public void Disconnect()
            {
                controller.WeaponChanched -= OnPickedUp;
                DetachWeapon(controller.CurrentWeapon);
                attackHandler = null;
                reloadingHandler = null;
            }

            private void DetachWeapon(Weapon weapon)
            {
                if (weapon)
                {
                    weapon.Attacked -= attackHandler;
                    DistanceWeapon distanceWeapon = weapon as DistanceWeapon;
                    if (distanceWeapon != null)
                    {
                        distanceWeapon.MagazineReloaded -= reloadingHandler;
                    }
                }
            }
        }
    }
}
