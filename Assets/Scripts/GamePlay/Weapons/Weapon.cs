using System;
using System.Collections;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;

namespace Gameplay
{
    namespace Weapons
    {
        public abstract class Weapon : NetworkBehaviour
        {
            [SerializeField] private string nameOfAttack;
            [SerializeField]
            [Tooltip("Time is exressed in seconds")]
            private float reloadingTime;
            private bool isReloading;
            public event UnityAction<string> Attacked;
            private NetworkVariable<bool> isActive;

            private void Awake()
            {
                isActive = new NetworkVariable<bool>(true, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Server);
                isActive.OnValueChanged += SetActive;
            }

            private void SetActive(bool prev, bool current)
            {
                gameObject.SetActive(current);
            }

            [ServerRpc(RequireOwnership = false)]
            public virtual void AttackServerRpc()
            {
                if (isReloading)
                    return;
                if (TryAttack())
                {
                    Attacked?.Invoke(nameOfAttack);
                    isReloading = true;
                    NetworkManager.StartCoroutine(ReloadCoroutine());
                }
            }

            protected void StartValidation()
            {
                Debug.Log($"Weapon {gameObject.name}; ID {NetworkObjectId} is {isActive.Value}");
                gameObject.SetActive(isActive.Value);
            }

            protected abstract bool TryAttack();

            private IEnumerator ReloadCoroutine()
            {
                yield return new WaitForSeconds(reloadingTime);
                isReloading = false;
            }

            public void DropShootEvent()
            {
                if (Attacked != null)
                {
                    Delegate[] methods = Attacked.GetInvocationList();
                    for (int i = 0; i < methods.Length; i++)
                    {
                        Attacked -= methods[i] as UnityAction<string>;
                    }
                }
            }

            public string AttackName => nameOfAttack;

            public bool IsActive
            {
                get => isActive.Value;
                set
                {
                    if (IsClient)
                    {
                        SetActiveServerRpc(value, NetworkObjectId);
                    }
                    else
                    {
                        isActive.Value = value;
                    }
                }
            }

            [ServerRpc(RequireOwnership = false)]
            private void SetActiveServerRpc(bool state, ulong weaponId)
            {
                NetworkObject weaponObj = GetNetworkObject(weaponId);
                if (weaponObj)
                {
                    Weapon weapon = weaponObj.GetComponent<Weapon>();
                    weapon.isActive.Value = state;
                }
            }
        }
    }
}


