using Unity.Netcode;
using UnityEngine;

namespace Gameplay
{
    namespace Weapons
    {
        public class PoolingWeapon : DistanceWeapon
        {
            [SerializeField]
            private NetCodePoolingList<Ammo> ammo;
            [SerializeField] private Ammo ammoPrefab;

            private void Start()
            {
                ammo = new NetCodePoolingList<Ammo>(Magazine, ammoPrefab, gameObject, NetworkObject.OwnerClientId);
                StartValidation();
                if (IsClient)
                {
                    PoolAmmoServerRpc(NetworkObjectId);
                }
            }

            [ServerRpc(RequireOwnership = false)]
            private void PoolAmmoServerRpc(ulong objId)
            {
                NetworkObject weaponObj = GetNetworkObject(objId);
                PoolingWeapon weapon = weaponObj.gameObject.GetComponent<PoolingWeapon>();
                weapon.ammo.Pool();
            }

            protected override void Shoot()
            {
                ammo.GetNext().BecomeActive();
            }
        }
    }
}
