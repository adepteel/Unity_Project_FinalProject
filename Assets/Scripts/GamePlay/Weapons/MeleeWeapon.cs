using UnityEngine;

namespace Gameplay
{
    using Creatures;

    namespace Weapons
    {
        public sealed class MeleeWeapon : Weapon
        {
            [SerializeField]
            private int damage;
            private GameObject enemy;

            protected override bool TryAttack()
            {            
                ILivingCreature livingCreature = enemy.GetComponent<ILivingCreature>();
                livingCreature.HP -= damage;
                return true;
            }


            private void OnCollisionEnter(Collision collision)
            {
                if (collision.gameObject.CompareTag("Creature"))
                {
                    enemy = collision.gameObject;
                }
            }
        }
    }
}


