using Unity.Netcode;
using UnityEngine;

namespace Gameplay
{
    namespace Weapons
    {
        public class Ammo : NetworkBehaviour, IPooledObject
        {
            private GameObject parent;
            private DistanceWeapon parentWeapon;
            [SerializeField] private float speed;
            private NetworkVariable<bool> isActive;

            private void Awake()
            {
                isActive = new NetworkVariable<bool>(true, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Server);
                isActive.OnValueChanged += SetActive;
            }

            private void SetActive(bool prev, bool current)
            {
                gameObject.SetActive(current);
            }

            private void Start()
            {
                gameObject.SetActive(isActive.Value);
            }

            private void FixedUpdate()
            {
                if (IsServer)
                {
                    if (isActiveAndEnabled) 
                        Fly(NetworkObjectId);
                }
            }

            public void BecomeActive()
            {
                isActive.Value = true;
                transform.parent = null;
            }

            private void Fly(ulong objId)
            {
                NetworkObject ammo = GetNetworkObject(objId);
                if (ammo != null)
                {
                    float speed = ammo.GetComponent<Ammo>().speed;
                    ammo.transform.position += ammo.transform.forward * speed * Time.fixedDeltaTime;
                }
            }

            public void PushBack()
            {
                isActive.Value = false;
                NetworkObject.TrySetParent(parent.transform);
                transform.position = parentWeapon.BarrelEnd.position;
                transform.rotation = parentWeapon.BarrelEnd.rotation;
            }
        
            public GameObject Parent { 
                get => parent;
                set 
                {
                    parentWeapon ??= value.GetComponentInChildren<DistanceWeapon>(true);
                    if (parent == null)
                    {
                        parent = FindParentWithNetworkObject(value);
                    }
                }
            }

            private GameObject FindParentWithNetworkObject(GameObject gameObject)
            {
                if (gameObject.TryGetComponent(out NetworkObject parent)) 
                {
                    return parent.gameObject;
                }
                else
                {
                    FindParentWithNetworkObject(gameObject.transform.parent.gameObject);
                }
                return null;
            }
        }
    }
}


