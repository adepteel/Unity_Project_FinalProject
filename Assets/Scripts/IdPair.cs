using Unity.Netcode;

public struct IdPair: INetworkSerializable 
{
    public int LocalId;
    public ulong NetId;

    public IdPair(in int first, in ulong second)
    {
        LocalId = first;
        NetId = second;
    }

    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref LocalId);
        serializer.SerializeValue(ref NetId);
    }

    public override string ToString()
    {
        return $"LocalId: {LocalId}| NetId: {NetId}";
    }
}
